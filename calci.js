class ScientificCalculator {
    constructor(_angka1) {
        this.angka1 = _angka1;
    }

    add(angka) {
        this.angka1 = this.angka1 + angka;
        return this.angka1;
    }

    substract(angka) {
        this.angka1 = this.angka1 - angka;
        return this.angka1;
    }

    devide(angka) {
        this.angka1 = this.angka1 / angka; 
        return this.angka;
    }

    multiply(angka) {
        this.angka1 = this.angka1 * angka;
        return this.angka1;
    }

    pi() {
        return Math.PI;
    }

    cos() {
        this.angka1 = Math.cos(this.angka1);
        return this.angka1
    }

    sin() {   
        this.angka1 =  Math.sin(this.angka1);
        return this.angka1;
    }

    tan() {
        this.angka1 = Math.tan(this.angka1);
        return this.angka1
    }

    cosh() {
        this.angka1 = Math.cosh(this.angka1);
        return this.angka1;
    }

    sinh() {
        this.angka1 = Math.sinh(this.angka1);
        return this.angka1;
    }

    tanh() {
        this.angka1 = Math.tanh(this.angka1); 
        return this.angka1;
    }

    exp() {
        this.angka1 = Math.exp(this.angka1);
        return this.angka1;
    }

    floor() {
        this.angka1 = Math.floor(this.angka1);
        return this.angka1;
    }

    power(angka) {
        this.angka1 = Math.pow(this.angka1, angka);
        return this.angka1;
    }

    squareRoot() {
        this.angka1 = Math.sqrt(this.angka1);
        return this.angka1;
    }

    logTwo() {
        this.angka1 = Math.log(this.angka1); 
        return this.angka1;
    }

    logTen() {
        this.angka1 = Math.log10(this.angka1);
        return this.angka1;
    }

    ceiling() {
        this.angka1 = Math.ceil(this.angka1);
        return this.angka1;
    }
}